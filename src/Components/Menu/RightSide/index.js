import React from 'react'

class RightSide extends React.PureComponent {
	render(){

		let { StyledMenu } = this.props

		return(
			<StyledMenu.Container>
				<p>RightSide</p>
			</StyledMenu.Container>
		)
	}
}

export default RightSide