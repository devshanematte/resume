import Menu from './Menu'
import Main from './Main'
import Text from './Text'
import Helpers from './Helpers'

const Styled = {
	Menu,
	Main,
	Text,
	Helpers
}

export default Styled