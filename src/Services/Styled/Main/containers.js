import styled from 'styled-components'

//Main.Containers

const bgImage = require('../../../assets/subtle_grunge.png')

const Container = styled.div`
    width:100%;
    position:relative;
    background:#c1c1c1;
    min-height:100vh;
    padding:15px 0;
`

const ContainerRow = styled.div`
    width:100%;
    display:flex;
    flex-direction:row;
    box-sizing:border-box;
    align-items:center;
    justify-content:${props => props.spaceBetween ? `space-between` : ''};
    padding:5px 0;
    border-radius:0px;
    box-shadow:0 0 10px rgba(0,0,0,0);
    transition:.3s all;

	&:p {
    	margin:0;
    }

    &:i {
    	margin-right:5px;
    }

    &:hover{
    	background:${props => props.hover ? `rgba(255,255,255,0.2)` : ''};
    	cursor:${props => props.hover ? `pointer` : ''};
    	box-shadow:${props => props.hover ? `0 0 15px rgba(0,0,0,0.1)` : ''};
    	padding:${props => props.hover ? `5px 10px` : ''};
    	border-radius:5px;
    	transition:.3s all;
    }
`

const ContainerColumn = styled.div`
    width:100%;
    display:flex;
    flex-direction:column;
    box-sizing:border-box;
    padding:${props => props.notPadding ? '0' : '5px 0'};
    margin-top:${props => props.marginTop ? `${props.marginTop}px` : '0'};
    margin-bottom:${props => props.marginBottom ? `${props.marginBottom}px` : '0'};
    border-left: ${props => props.leftBorder ? `4px solid rgba(51, 51, 51, 0.15)` : '0'};
    padding-left: ${props => props.leftBorder ? `25px` : '0'};
    position:relative;

    &:after {
	    content: '';
	    width: 10px;
	    height: 10px;
	    border-radius: 50%;
	    background: ${props => props.leftDot ? `rgb(117, 117, 117)` : ''};
	    top: 6px;
	    left: -32px;
	    position: absolute;
    }
`

const ContainerIcon = styled.div`
    margin-right:10px;
    padding:3px 10px 3px 0;
    border-right:1px solid rgba(0, 0, 0, 0.21);
    display:flex;
    align-items:center;

    .glyphicon {
    	color:#5c5c5c;
    	font-size:20px;
    }
`

const Content = styled.div`
    width:100%;
    background: url(${bgImage});
    box-sizing:border-box;
    padding:5px 55px;
    border-radius:1px;
    box-shadow: 0 10px 19px rgba(0, 0, 0, 0.16)
`

const Section = styled.section`
    width:${props => props.width ? `${props.width}px` : '100%'};
    display:table;
    margin-top:${props => props.marginTop ? `${props.marginTop}px` : '0'};
    margin-bottom:${props => props.marginBottom ? `${props.marginBottom}px` : '0'}
`

const LineBlock = styled.div`
    width:75%;
    height: 5px;
    background: rgba(0, 0, 0, 0.22);
    position:relative;

    &:after{
    	content:'';
    	display:block;
    	height:100%;
    	width:${props => props.width ? `${props.width}%` : ''};
    	position:absolute;
    	top:0;
    	left:0;
    	background:rgba(0, 0, 0, 0.36);
    	transition:.4s all;
    }
`

export default {
  Container,
  Content,
  Section,
  ContainerRow,
  ContainerIcon,
  ContainerColumn,
  LineBlock
}