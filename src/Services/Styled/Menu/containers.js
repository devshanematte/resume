import styled from 'styled-components'

const Container = styled.div`
    width:70px;
    height:100vh;
    position:fixed;
    top:0;
    left:0;
    z-index:1000;
    background:#f1f1f1;
`

export {
  Container
}