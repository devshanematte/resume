import React from 'react'
import PropTypes from 'prop-types'

class Experience extends React.PureComponent {
	render(){

		const { styled } = this.props

		return(
			<styled.Main.Containers.ContainerColumn notPadding leftBorder>
				
				<styled.Main.Containers.ContainerColumn leftDot notPadding marginTop={40}>
					<styled.Text.PI bold marginBottom={5} marginTop={1}>May 2020 - 2025</styled.Text.PI>
					<styled.Text.H3>Frontend, backend developer</styled.Text.H3>
					<styled.Text.a href="https://logovo.kz" italic target="blank" marginBottom={5}>logovo.kz</styled.Text.a>
					<styled.Text.P>Website and web application development (reactjs, nextjs)</styled.Text.P>
					<styled.Text.P>Server development (nodejs, koa2, docker, mongodb, graphql)</styled.Text.P>
				</styled.Main.Containers.ContainerColumn>

				<styled.Main.Containers.ContainerColumn leftDot notPadding marginTop={40}>
					<styled.Text.PI bold marginBottom={5} marginTop={1}>April 2015 - May 2018</styled.Text.PI>
					<styled.Text.H3>Frontend, backend developer</styled.Text.H3>
					<styled.Text.a href="https://motive.kz" italic target="blank" marginBottom={5}>motive.kz</styled.Text.a>
					<styled.Text.P>Website and web application development</styled.Text.P>
					<styled.Text.P>Server development (nodejs/koa2)</styled.Text.P>
				</styled.Main.Containers.ContainerColumn>

				<styled.Main.Containers.ContainerColumn leftDot notPadding marginTop={40}>
					<styled.Text.PI bold marginBottom={5} marginTop={1}>September 2013 - Present</styled.Text.PI>
					<styled.Text.H3>Frontend, backend, mobile developer</styled.Text.H3>
					<styled.Text.PI marginBottom={5}>freelance</styled.Text.PI>
					<styled.Text.P>Website and web application development</styled.Text.P>
					<styled.Text.P>Mobile application development</styled.Text.P>
					<styled.Text.P>Server development (nodejs/koa2)</styled.Text.P>
				</styled.Main.Containers.ContainerColumn>

				<styled.Main.Containers.ContainerColumn leftDot notPadding marginTop={40}>
					<styled.Text.PI bold marginBottom={5} marginTop={1}>October 2012 - September 2013</styled.Text.PI>
					<styled.Text.H3>Frontend developer</styled.Text.H3>
					<styled.Text.a href="https://ir.kz" italic target="blank" marginBottom={5}>ir.kz</styled.Text.a>
					<styled.Text.P>Website and web application development</styled.Text.P>
					<styled.Text.P>Optimisation</styled.Text.P>
					<styled.Text.P>Finding solutions and ideas</styled.Text.P>
				</styled.Main.Containers.ContainerColumn>

				<styled.Main.Containers.ContainerColumn leftDot notPadding>
					<styled.Text.PI bold marginBottom={5} marginTop={1}>October 2010 - May 2012</styled.Text.PI>
					<styled.Text.H3>Backend developer</styled.Text.H3>
					<styled.Text.a href="https://blizzard.kz" italic target="blank" marginBottom={5}>blizzard.kz</styled.Text.a>
					<styled.Text.P>Project development and support blizzard.kz</styled.Text.P>
					<styled.Text.P>Optimisation</styled.Text.P>
				</styled.Main.Containers.ContainerColumn>

			</styled.Main.Containers.ContainerColumn>
		)
	}
}

Experience.propTypes = {
  styled: PropTypes.object
}

export default Experience