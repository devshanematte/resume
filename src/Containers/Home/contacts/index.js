import React from 'react'
import PropTypes from 'prop-types'

class Contacts extends React.PureComponent {
	render(){

		const { styled } = this.props

		return(
			<styled.Main.Containers.Section marginBottom={10}>

				<styled.Main.Containers.ContainerRow>
					<styled.Main.Containers.ContainerIcon>
						<i className="glyphicon glyphicon-phone-alt"></i>
					</styled.Main.Containers.ContainerIcon>
					<styled.Text.a normal href="tel:+7 778 449 79 06">+7 778 449 79 06</styled.Text.a>
				</styled.Main.Containers.ContainerRow>

				<styled.Helpers.Devider black width={50} marginTop={5} marginBottom={5} />

				<styled.Main.Containers.ContainerRow>
					<styled.Main.Containers.ContainerIcon>
						<i className="glyphicon glyphicon-envelope"></i>
					</styled.Main.Containers.ContainerIcon>
					<styled.Text.a normal href="mailto:devshanematte@gmail.com">devshanematte@gmail.com</styled.Text.a>
				</styled.Main.Containers.ContainerRow>

				<styled.Helpers.Devider black width={50} marginTop={5} marginBottom={5} />

				<styled.Main.Containers.ContainerRow>
					<styled.Main.Containers.ContainerIcon>
						<i className="glyphicon glyphicon-send"></i>
					</styled.Main.Containers.ContainerIcon>
					<styled.Text.a normal target="blank" href="https://t.me/Shanematte">@Shanematte</styled.Text.a>
				</styled.Main.Containers.ContainerRow>

			</styled.Main.Containers.Section>
		)
	}
}

Contacts.propTypes = {
  styled: PropTypes.object
}

export default Contacts