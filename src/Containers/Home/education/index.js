import React from 'react'
import PropTypes from 'prop-types'

class Education extends React.PureComponent {
	render(){

		const { styled } = this.props

		return(
			<styled.Main.Containers.ContainerColumn>
				
				<styled.Main.Containers.ContainerColumn marginBottom={7}>
					<styled.Text.H3>Computer Science and Software</styled.Text.H3>
					<styled.Text.PI>polytechnic college</styled.Text.PI>
					<styled.Text.P bold>2008 - 2010</styled.Text.P>
				</styled.Main.Containers.ContainerColumn>

			</styled.Main.Containers.ContainerColumn>
		)
	}
}

Education.propTypes = {
  styled: PropTypes.object
}

export default Education