import React from 'react'
import PropTypes from 'prop-types'

class Skills extends React.PureComponent {
	render(){

		const { styled } = this.props

		return(
			<styled.Main.Containers.ContainerColumn>

				<styled.Main.Containers.ContainerRow hover spaceBetween>
					<styled.Main.Containers.Section width={150}>
						<styled.Text.P >Javascript</styled.Text.P>
					</styled.Main.Containers.Section>
					<styled.Main.Containers.LineBlock width={100}/>
				</styled.Main.Containers.ContainerRow>

				<styled.Main.Containers.ContainerRow hover spaceBetween>
					<styled.Main.Containers.Section width={150}>
						<styled.Text.P >Typescript</styled.Text.P>
					</styled.Main.Containers.Section>
					<styled.Main.Containers.LineBlock width={100}/>
				</styled.Main.Containers.ContainerRow>

				<styled.Main.Containers.ContainerRow hover spaceBetween>
					<styled.Main.Containers.Section width={150}>
						<styled.Text.P >Docker</styled.Text.P>
					</styled.Main.Containers.Section>
					<styled.Main.Containers.LineBlock width={100}/>
				</styled.Main.Containers.ContainerRow>

				<styled.Main.Containers.ContainerRow hover spaceBetween>
					<styled.Main.Containers.Section width={150}>
						<styled.Text.P >GraphQL</styled.Text.P>
					</styled.Main.Containers.Section>
					<styled.Main.Containers.LineBlock width={100}/>
				</styled.Main.Containers.ContainerRow>

				<styled.Main.Containers.ContainerRow hover spaceBetween>
					<styled.Main.Containers.Section width={150}>
						<styled.Text.P >Apollo</styled.Text.P>
					</styled.Main.Containers.Section>
					<styled.Main.Containers.LineBlock width={100}/>
				</styled.Main.Containers.ContainerRow>

				<styled.Main.Containers.ContainerRow hover spaceBetween>
					<styled.Main.Containers.Section width={150}>
						<styled.Text.P >HTML/CSS</styled.Text.P>
					</styled.Main.Containers.Section>
					<styled.Main.Containers.LineBlock width={100}/>
				</styled.Main.Containers.ContainerRow>

				<styled.Main.Containers.ContainerRow hover spaceBetween>
					<styled.Main.Containers.Section width={150}>
						<styled.Text.P >GIT</styled.Text.P>
					</styled.Main.Containers.Section>
					<styled.Main.Containers.LineBlock width={100}/>
				</styled.Main.Containers.ContainerRow>

				<styled.Main.Containers.ContainerRow hover spaceBetween>
					<styled.Main.Containers.Section width={150}>
						<styled.Text.P >nextjs</styled.Text.P>
					</styled.Main.Containers.Section>
					<styled.Main.Containers.LineBlock width={100}/>
				</styled.Main.Containers.ContainerRow>

				<styled.Main.Containers.ContainerRow hover spaceBetween>
					<styled.Main.Containers.Section width={150}>
						<styled.Text.P >React/react native</styled.Text.P>
					</styled.Main.Containers.Section>
					<styled.Main.Containers.LineBlock width={100}/>
				</styled.Main.Containers.ContainerRow>

				<styled.Main.Containers.ContainerRow hover spaceBetween>
					<styled.Main.Containers.Section width={150}>
						<styled.Text.P >Redux TK</styled.Text.P>
					</styled.Main.Containers.Section>
					<styled.Main.Containers.LineBlock width={100}/>
				</styled.Main.Containers.ContainerRow>

				<styled.Main.Containers.ContainerRow hover spaceBetween>
					<styled.Main.Containers.Section width={150}>
						<styled.Text.P >Services/APIs</styled.Text.P>
					</styled.Main.Containers.Section>
					<styled.Main.Containers.LineBlock width={100}/>
				</styled.Main.Containers.ContainerRow>

				<styled.Main.Containers.ContainerRow hover spaceBetween>
					<styled.Main.Containers.Section width={150}>
						<styled.Text.P >Nodejs</styled.Text.P>
					</styled.Main.Containers.Section>
					<styled.Main.Containers.LineBlock width={100}/>
				</styled.Main.Containers.ContainerRow>

				<styled.Main.Containers.ContainerRow hover spaceBetween>
					<styled.Main.Containers.Section width={150}>
						<styled.Text.P >Nestjs</styled.Text.P>
					</styled.Main.Containers.Section>
					<styled.Main.Containers.LineBlock width={100}/>
				</styled.Main.Containers.ContainerRow>

				<styled.Main.Containers.ContainerRow hover spaceBetween>
					<styled.Main.Containers.Section width={150}>
						<styled.Text.P >MongoDB</styled.Text.P>
					</styled.Main.Containers.Section>
					<styled.Main.Containers.LineBlock width={100}/>
				</styled.Main.Containers.ContainerRow>

				<styled.Main.Containers.ContainerRow hover spaceBetween>
					<styled.Main.Containers.Section width={150}>
						<styled.Text.P >Nginx</styled.Text.P>
					</styled.Main.Containers.Section>
					<styled.Main.Containers.LineBlock width={100}/>
				</styled.Main.Containers.ContainerRow>

			</styled.Main.Containers.ContainerColumn>
		)
	}
}

Skills.propTypes = {
  styled: PropTypes.object
}

export default Skills