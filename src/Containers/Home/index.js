import React from 'react'
import { connect } from 'react-redux'

import Styled from '../../Services/Styled'

import Contacts from './contacts'
import Education from './education'
import Skills from './skills'
import Experience from './experience'
import moment from 'moment'

class Home extends React.Component {
	render(){
		return(
			<Styled.Main.Containers.Container>
				
				<div className="container">
					<Styled.Main.Containers.Content>

						<Styled.Main.Containers.Section marginBottom={40} marginTop={50}>
							<Styled.Text.H1 transform>Nikolay Erofeev</Styled.Text.H1>
							<Styled.Text.P transform>frontend and backend developer</Styled.Text.P>
						</Styled.Main.Containers.Section>

						<Styled.Helpers.Devider/>

						<Styled.Main.Containers.Section marginBottom={50} marginTop={40}>
							<Styled.Text.H2 transform>Profile</Styled.Text.H2>
							<Styled.Text.P normal>I started programming back in 2010. I started with the layout of sites, then the backend. I studied php. Started to write servers on kohana, laravel. Used jquery on the client. After 2 years of experience I decided to switch completely to nodejs. Started to study nodejs in terms of server programming. I used express as a backend, but after the Koa2 presentation I switched to Koa. I remember using a jade on frontend. After a while I learned about react from facebook. I tried it, I liked the component approach very much. I tried the Vuejs, but the applications all started to write on reactjs. In 2015, facebook presented react native. React native was very good in terms of rendering and performance. Today use koa2 on the server. I'm using Redux TK as the global state . I use react native in mobile development. Behind the back more than 6 years in web development, and 2 years in mobile development. Plans to learn golang from google and dart.</Styled.Text.P>
						</Styled.Main.Containers.Section>

						<Styled.Helpers.Devider/>

						<div className="row">
							<div className="col-md-6">

								<Styled.Main.Containers.ContainerColumn marginBottom={50} marginTop={40}>
									<Styled.Text.H2 transform>Contacts</Styled.Text.H2>
									<Contacts styled={Styled} />
								</Styled.Main.Containers.ContainerColumn>

								<Styled.Main.Containers.ContainerColumn marginBottom={50} marginTop={40}>
									<Styled.Text.H2 transform>Education</Styled.Text.H2>
									<Education styled={Styled} />
								</Styled.Main.Containers.ContainerColumn>

								<Styled.Main.Containers.ContainerColumn marginBottom={50} marginTop={40}>
									<Styled.Text.H2 transform>Skills</Styled.Text.H2>
									<Skills styled={Styled} />
								</Styled.Main.Containers.ContainerColumn>

							</div>
							<div className="col-md-6">

								<Styled.Main.Containers.ContainerColumn marginBottom={50} marginTop={40}>
									<Styled.Text.H2 transform>Experience</Styled.Text.H2>
									<Experience styled={Styled} />
								</Styled.Main.Containers.ContainerColumn>

							</div>
						</div>

						<Styled.Helpers.Devider/>

						<div className="row">

							<div className="col-md-3">

								<Styled.Main.Containers.Section marginBottom={20} marginTop={30}>
									
								</Styled.Main.Containers.Section>

							</div>

							<div className="col-md-3">

								<Styled.Main.Containers.Section marginBottom={20} marginTop={30}>

								</Styled.Main.Containers.Section>

							</div>

							<div className="col-md-3">

								<Styled.Main.Containers.Section marginBottom={20} marginTop={30}>

								</Styled.Main.Containers.Section>

							</div>

							<div className="col-md-3">

								<Styled.Main.Containers.Section marginBottom={20} marginTop={30}>
									<Styled.Text.P align="right" bold>© { moment().format('YYYY') }</Styled.Text.P>
								</Styled.Main.Containers.Section>

							</div>

						</div>

					</Styled.Main.Containers.Content>
				</div>

			</Styled.Main.Containers.Container>
		)
	}
}

const mapStateToProps = (state) => {
	return {

	}
}

export default connect(mapStateToProps)(Home)