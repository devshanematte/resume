import React from 'react'
import { Provider } from 'react-redux'
import createStore from '../Services/Store'
import { PersistGate } from 'redux-persist/lib/integration/react'

import Home from './Home'

class App extends React.Component {
	render(){

		const {
			store,
			persistor
		} = createStore()

		return(
			<Provider store={store}>
				<PersistGate persistor={persistor}>
					<Home/>
				</PersistGate>
			</Provider>
		)
	}
}

export default App